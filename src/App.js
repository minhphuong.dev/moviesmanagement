import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import Loading from './components/Loading';

// Cần check lại sau khi fetch user
const isAuthenticated = localStorage.getItem("_user") !== null

// Containers
const Dashboard = React.lazy(() => import('./views/Dashboard'));

// Pages
const LogInSide = React.lazy(() => import('./views/Login'));
const RegisterSide = React.lazy(() => import('./views/Register'));

class App extends Component {
  render() {
    return (
      <HashRouter>
        <React.Suspense fallback={Loading()}>
          <Switch>
            <Route exact path="/login" name="Login Page" render={props => <LogInSide {...props} />} />
            <Route exact path="/register" name="Register Page" render={props => <RegisterSide {...props} />} />
            <Route path="/" name="Dashboard" render={props => <Dashboard {...props} />} />
          </Switch>
          <Redirect from="/" to={isAuthenticated ? "/" : "/login"} />
        </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;