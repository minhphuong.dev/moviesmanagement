import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { callApi } from '../utils/apiCaller';

export const fetchMoviesAsync = createAsyncThunk(
	'movie/fetch',
	async ({ limit, offset }, { rejectWithValue }) => {
		try {
			const response = await callApi(`movies?limit=${limit || 10}&offset=${offset || 0}`)
			return response?.data
		} catch (e) {
			return rejectWithValue(e?.message)
		}
	}
)

const slice = createSlice({
	name: 'movie',
	initialState: {
		isLoading: false,
		data: {
      results: [],
      total: 0
    },
		errorMessage: ''
	},
	reducers: { },
	extraReducers: {
		[fetchMoviesAsync.pending]: (state, action) => {
			state.isLoading = true;
			state.errorMessage = '';
		},
		[fetchMoviesAsync.fulfilled]: (state, action) => {
			state.data = action.payload;
			state.isLoading = false;
			state.errorMessage = '';
		},
		[fetchMoviesAsync.rejected]: (state, action) => {
			state.errorMessage = action.payload;
			state.isLoading = false;
		},
	}
});

export default slice.reducer