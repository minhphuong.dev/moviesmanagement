import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import thunk from "redux-thunk";
import authSlice from './authSlice'
import moviesSlice from './moviesSlice'

const store = configureStore({
    reducer: {
        auth: authSlice,
        movie: moviesSlice,
    },
    middleware: [thunk, logger]
});

export default store;