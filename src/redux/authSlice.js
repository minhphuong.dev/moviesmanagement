import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { callApi } from '../utils/apiCaller';

export const loginAsync = createAsyncThunk(
	'auth/login',
	async ({ email, password }, { rejectWithValue }) => {
		try {
			const response = await callApi('auth/login', 'POST', { email, password })
			localStorage.setItem('_user', JSON.stringify(response?.data))
			return response?.data
		} catch (e) {
			return rejectWithValue('Email or password is incorrect!')
		}
	}
)

export const registerAsync = createAsyncThunk(
	'auth/register',
	async ({ email, password, firstName, lastName, city, country, phoneNumber, postCode, address }, { rejectWithValue }) => {
		try {
			const response = await callApi('auth/register', 'POST', { email, password, firstName, lastName, city, country, phoneNumber, postCode, address })
			localStorage.setItem('_user', JSON.stringify(response?.data))
			return response?.data
		} catch (e) {
			return rejectWithValue('Register unsuccess! Please input all field!')
		}
	}
)

const slice = createSlice({
	name: 'auth',
	initialState: {
		isLoading: false,
		user: JSON.parse(localStorage.getItem('_user')),
		errorMessage: ''
	},
	reducers: {
		logoutSuccess: (state, action) => {
			state.user = null;
		}
	},
	extraReducers: {
		[loginAsync.pending]: (state, action) => {
			state.isLoading = true;
			state.errorMessage = '';
		},
		[loginAsync.fulfilled]: (state, action) => {
			state.user = action.payload;
			state.isLoading = false;
			state.errorMessage = '';
		},
		[loginAsync.rejected]: (state, action) => {
			state.errorMessage = action.payload;
			state.isLoading = false;
		},
		[registerAsync.pending]: (state, action) => {
			state.isLoading = true;
			state.errorMessage = '';
		},
		[registerAsync.fulfilled]: (state, action) => {
			state.user = action.payload;
			state.isLoading = false;
			state.errorMessage = '';
		},
		[registerAsync.rejected]: (state, action) => {
			state.errorMessage = action.payload;
			state.isLoading = false;
		},
	}
});

export default slice.reducer

const { logoutSuccess } = slice.actions

export const logout = () => dispatch => {
  try {
		localStorage.removeItem('_user')
    return dispatch(logoutSuccess())
  } catch (e) {
    return console.error(e.message);
  }
}