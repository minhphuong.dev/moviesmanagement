import * as Config from '../api/configs';
import axios from 'axios';

export const callApi = (
  endpoint = '',
  method = 'GET',
  body = null
) => {
  try {
    let configs = {
      method: method,
      url: `${Config.BASE_URL}/${endpoint}`,
      data: body,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem("_user"))?.token || ''}`,
      },
    };
    return axios(configs);
  } catch (e) {
  }
};