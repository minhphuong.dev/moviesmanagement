import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Form } from 'react-bootstrap';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router';
import { registerAsync } from '../../redux/authSlice';
import { connect } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    fontFamily: 'Open Sans',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
    fontFamily: 'Open Sans',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    borderRadius: '21.5px',
    backgroundColor: '#454B60',
  },
  title: {
    alignSelf: 'flex-start',
  },
  errorMessage: {
    alignSelf: 'flex-start',
    color: 'red'
  }
}));

const RegisterSide = ({ auth, register }) => {
  const classes = useStyles();

  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [postCode, setPostCode] = useState("")
  const [address, setAddress] = useState("")
  const [city, setCity] = useState("")
  const [country, setCountry] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")

  const handleRegister = (e) => {
    e.preventDefault();
    register({ firstName, lastName, phoneNumber, postCode, address, city, country, email, password, confirmPassword })
  }

  if (auth.user) {
    return <Redirect to='/' />
  }

  return (
    <Grid container component="main" className={classes.root}>
      <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5" style={useStyles.title} className={classes.title}>
            Sign Up
            </Typography>
          <Typography component="h6" variant="h6" className={classes.errorMessage}>
            {auth?.errorMessage || ''}
          </Typography>
          <form className={classes.form} noValidate>
            <Form>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={firstName}
                      onChange={(e) => setFirstName(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={lastName}
                      onChange={(e) => setLastName(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={phoneNumber}
                      onChange={(e) => setPhoneNumber(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Post Code</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={postCode}
                      onChange={(e) => setPostCode(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
              </Grid>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder=""
                  defaultValue={address}
                  onChange={(e) => setAddress(e.target.value)}
                />
              </Form.Group>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>City</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={city}
                      onChange={(e) => setCity(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Country</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      defaultValue={country}
                      onChange={(e) => setCountry(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                </Grid>
              </Grid>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder=""
                  defaultValue={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder=""
                  defaultValue={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder=""
                  defaultValue={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </Form.Group>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleRegister}
              >
                Sign Up
              </Button>
            </Form>
          </form>
          <Grid item>
            <Link href="/#/login" variant="body2">
              {"Already have an account? Sign In"}
            </Link>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={false} sm={4} md={9} className={classes.image} />
    </Grid>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = dispatch => {
  return {
    register: (params) => dispatch(registerAsync(params))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterSide)