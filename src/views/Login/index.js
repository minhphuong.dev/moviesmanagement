import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Form } from 'react-bootstrap';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { loginAsync } from '../../redux/authSlice'
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    fontFamily: 'Open Sans sans-serif',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    fontFamily: 'Open Sans',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    borderRadius: '21.5px',
    backgroundColor: '#454B60',
  },
  remember: {
    fontFamily: 'Open Sans',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '15px',
    lineHeight: '30px',
  },
  title: {
    alignSelf: 'flex-start',
  },
  errorMessage: {
    alignSelf: 'flex-start',
    color: 'red'
  }
}));

const LogInSide = ({ auth, login }) => {
  const classes = useStyles();

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const handleLogin = (e) => {
    e.preventDefault();
    login({ email, password })
  }

  if (auth.user) {
    return <Redirect to='/' />
  }

  return (
    <>
      <Grid container component="main" className={classes.root}>
        <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Typography component="h4" variant="h4" className={classes.title}>
              Log in
            </Typography>
            <Typography component="h6" variant="h6" className={classes.errorMessage}>
              { auth?.errorMessage || '' }
            </Typography>
            <Form className={classes.form}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder=""
                  defaultValue={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder=""
                  defaultValue={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Grid container>
                <Grid item xs={6}>
                  <Form.Group controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Remember me" />
                  </Form.Group>
                </Grid>
                <Grid item xs={6}>
                  <Link>Forgot password?</Link>
                </Grid>
              </Grid>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleLogin}
              >
                Log In
              </Button>
            </Form>
            <Grid item>
              <Link href="/#/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={false} sm={4} md={9} className={classes.image} />
      </Grid>
    </>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: (params) => dispatch(loginAsync(params))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogInSide)