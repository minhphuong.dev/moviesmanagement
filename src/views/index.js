import React from 'react'
import {
  BrowserRouter,
  Route,
} from 'react-router-dom'

import Login from './Login';
import Register from './Register';
import Dashboard from './Dashboard';

export default () => (
  <BrowserRouter>
    <React.Fragment>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
    </React.Fragment>
  </BrowserRouter>
)