import React, { useEffect, useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { connect } from 'react-redux';
import { fetchMoviesAsync } from '../../../redux/moviesSlice';

const Movies = ({ movie, fetchMovies }) => {
  const [limit, setLimit] = useState(10)
  const [offset, setOffset] = useState(0)

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 100,
    },
    {
      field: 'thumbnail.src',
      headerName: 'Image',
      width: 200,
    },
    {
      field: 'name',
      headerName: 'Name',
      width: 200,
    },
    {
      field: 'releaseDate',
      headerName: 'Time',
      width: 200,
    },
    {
      field: 'description',
      headerName: 'Description',
      width: 400,
    }
  ]

  const handlePageChange = (pagination) => {
    setOffset(pagination.page)
    setLimit(pagination.pageSize)
    fetchMovies({ limit, offset })
  }

  useEffect(() => {
    fetchMovies({ limit, offset })
  }, []);

  return (
    <React.Fragment>
      <div style={{ height: 600, width: '100%' }}>
        <DataGrid
          rows={movie.data.results}
          columns={columns}
          pagination
          pageSize={limit}
          rowsPerPageOptions={[5, 10, 20, 50, 100]}
          onPageSizeChange={handlePageChange}
          rowCount={movie.data.total}
          paginationMode="server"
          onPageChange={handlePageChange}
          loading={movie.isLoading}
        />
      </div>
    </React.Fragment>
  );
}

const mapStateToProps = state => {
  return {
    movie: state.movie
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchMovies: (params) => dispatch(fetchMoviesAsync(params))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies)