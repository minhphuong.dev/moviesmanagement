import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PermMediaIcon from '@material-ui/icons/PermMedia';

export const mainListItems = (
  <div>
    <ListItem button>
      <ListItemIcon>
        <PermMediaIcon />
      </ListItemIcon>
      <ListItemText primary="Movies" />
    </ListItem>
  </div>
);